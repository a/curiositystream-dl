# curiositystream-dl, a tool to download videos from curiositystream
# Copyright (C) 2019 Ave

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import json
import logging
import requests
from slugify import slugify
from urllib.parse import quote

# import m3u8


def do_login(email, password, token=""):
    """Does login with given email and password, returns auth token."""
    if token:
        return token

    resp = requests.post(
        f"https://api.curiositystream.com/v1/login/?platform=google&email={quote(email)}&password={quote(password)}"
    )
    respj = resp.json()
    if "error" in respj:
        logging.error(respj["error"]["message"]["base"][0])
        sys.exit(2)
    return respj["message"]["auth_token"]


def download_file(url, filename):
    # Based on http://masnun.com/2016/09/18/
    # python-using-the-requests-module-to-download-large-files-efficiently.html
    resp = requests.get(url, stream=True)
    if resp.status_code != 200:
        logging.error(f"Failed download for {url}")
        return False
    logging.debug(f"Downloading: {url}")
    handle = open(filename, "wb")
    for chunk in resp.iter_content(chunk_size=512):
        if chunk:  # filter out keep-alive new chunks
            handle.write(chunk)
    return True


def get_slug(video_id):
    resp = requests.get(f"https://api.curiositystream.com/v1/media/{video_id}")
    respj = resp.json()

    # Account for the case of the video not existing
    if "error" in respj:
        logging.error(respj["error"]["message"])
        sys.exit(3)

    respd = respj["data"]
    name = f"{respd['display_tag']} {respd['title']}".strip()
    return slugify(name)


def get_mp4(video_id, auth_token):
    resp = requests.get(
        f"https://api.curiositystream.com/v1/media/{video_id}?showEncodings=Android",
        headers={"x-auth-token": auth_token},
    )
    respj = resp.json()
    # TODO: can we account for 1+ encodings?
    return respj["data"]["encodings"][-1]["file_url"]


def get_subtitle(video_id):
    resp = requests.get(f"https://api.curiositystream.com/v1/media/{video_id}")
    respj = resp.json()

    # Return false if no subs are available
    if "closed_captions" not in respj["data"] or not respj["data"]["closed_captions"]:
        return False

    # TODO: can we account for 1+ subtitles?
    return respj["data"]["closed_captions"][0]["file"]


# def get_playlist(video_id, auth_token):
#     resp = requests.get(
#         f"https://api.curiositystream.com/v1/media/{video_id}",
#         headers={"x-auth-token": auth_token},
#     )
#     respj = resp.json()
#     # TODO: can we account for 1+ encodings?
#     return respj["data"]["encodings"][0]["master_playlist_url"]


# def parse_master_playlist(m3u8_url):
#     m3u8_obj = m3u8.load(m3u8_url)
#     for playlist in m3u8_obj.playlists:
#         logging.debug(
#             f"bandwidth: {playlist.stream_info.bandwidth}, uri: {playlist.uri}"
#         )
#     # Change to 0 for lowest quality, -1 for highest
#     return m3u8_obj.playlists[-1].uri


# def parse_playlist(m3u8_url):
#     m3u8_obj = m3u8.load(m3u8_url)
#     url_base = "/".join(m3u8_url.split("/")[:-1])  # HACK
#     return [f"{url_base}/{key.uri}" for key in m3u8_obj.segments]


def download_video(video_id, auth_token):
    os.makedirs("out", exist_ok=True)

    # Fetch slug that will be used for filename
    slug = get_slug(video_id)

    logging.info(f"Fetched slug: {slug}")

    # Fetch playlists
    # master_playlist_url = get_playlist(video_id, auth_token)
    # playlist_url = parse_master_playlist(master_playlist_url)
    # playlist = parse_playlist(playlist_url)

    # out_path = os.path.join("out", f"{slug}.%(ext)s")
    # subprocess.run(f"youtube-dl {playlist} -o '{out_path}'", shell=True)

    # Download subtitle
    subtitle_url = get_subtitle(video_id)
    if subtitle_url:
        logging.info("Downloading subtitle.")
        subtitle_format = os.path.splitext(subtitle_url)[-1]
        out_path = os.path.join("out", f"{slug}{subtitle_format}")
        download_file(subtitle_url, out_path)
        logging.info("Downloaded subtitle.")

    # Download mp4
    mp4_url = get_mp4(video_id, auth_token)
    logging.info("Downloading video (mp4).")
    out_path = os.path.join("out", f"{slug}.mp4")
    download_file(mp4_url, out_path)
    logging.info("Downloaded video (mp4).")

    logging.info("Done downloading.")


if __name__ == "__main__":
    print("curiositystream-dl, a tool to download videos from curiositystream")
    print("GPLv3 | https://gitlab.com/a/curiositystream-dl")
    print(
        "Please do not use this software for illegal purposes like piracy, but rather for legal uses like personal backups (if it is legal in your jurisdiction-- it is in mine)."
    )
    logging.basicConfig(level=logging.INFO)

    # Account for no config
    if not os.path.exists("config.json"):
        logging.error(
            "No config found, please create one based on config.template.json."
        )
        sys.exit(1)

    # Read config
    with open("config.json", "r") as f:
        config = json.load(f)

    # Do auth, save token if we used email/password
    auth_token = do_login(config["email"], config["password"], config["token"])
    if config["token"] != auth_token:
        config["token"] = auth_token

        with open("config.json", "w") as f:
            json.dump(config, f)
        logging.info("Login successful, saved token, will use that from now on.")

    if len(sys.argv) <= 1:
        logging.error("No video ID supplied.")

    download_video(sys.argv[1], auth_token)
