# CuriosityStream-dl

A tool to download videos from CuriosityStream. Requires a subscription, obviously.

## Setup

- Clone or download the repo
- Install python3.6 or higher if you don't have it installed already
- Install the requirements on `requirements.txt` with `pip3 install -Ur requirements.txt --user`
- Copy `config.template.json` to `config.json`, fill in either `email` and `password` or `token`. If you only fill in `email` and `password`, `token` will be automatically filled when you first run the script.

## Usage

After going through setup, run it as `cs-dl.py video-id`.

Example: `python3.7 cs-dl.py 2998`.

Video ID is part of the URL on the CuriosityStream website (example: `https://curiositystream.com/video/2998/touching-the-sun`).

## Personal note to CuriosityStream

Thank you for not having DRM. That's a genuinely good thing.

Also, thank you for offering a good service. I really enjoy watching stuff on your service.

## Development notes

I wrote this back in March 2019, and only am releasing it today in late September with cleaned up code.

I commented out a lot of code related to m3u8 as I switched to using MP4s (which involves acting like an Android phone), but I'm not removing them as they might be useful in case CS blocks this method of downloading things.

## Disclaimer

This project is not affiliated with CuriosityStream, it is also not endorsed by them.

Please do not use this software for illegal purposes like piracy, but rather for legal uses like personal backups (if it is legal in your jurisdiction-- it is in mine).

Usage of this tool might be a violation of CuriosityStream ToS, that's your responsibility.

I don't accept any responsibility whatsoever.

